#!/bin/bash

export C_PATH="$LANG_PATH/dist"

################################################################################

ronin_require_libc () {
    echo hello world >/dev/null
}

ronin_include_libc () {
    motd_text "    -> lib C   : "$C_PATH
}

################################################################################

ronin_setup_libc () {
    echo hello world >/dev/null
}

